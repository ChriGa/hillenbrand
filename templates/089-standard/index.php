<?php
/**
 * @author   	cg@089webdesign.de
 */
 

defined('_JEXEC') or die;
//include system
include_once(JPATH_ROOT . "/templates/" . $this->template . '/lib/system.php');
//include template Functions CG
include_once(JPATH_ROOT . "/templates/" . $this->template . '/template_functions.php');
?>
<!DOCTYPE html>
<html lang="de-DE">
<head>
	<?php //CG: weitere Fonts zuerst via prefetch oder preload HIER einfügen dann fontface in CSS
	/*
		<link rel="prefetch" as="font" crossorigin="crossorigin" type="font/ttf" href="/templates/089-standard/fonts/montserrat-regular-webfont.ttf">
		<link rel="prefetch" as="font" crossorigin="crossorigin" type="font/ttf" href="/templates/089-template/fonts/XXX.woff2">		
	*/ ?>	

	<?php 
	// including head
	include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/head.php');
	?>
	<script type="text/javascript">
		<?php //CG neuladen oder refresh immer von oben beginen: ?>
			jQuery(function(){
				jQuery(this).scrollTop(0);
			});
	</script>
	<link href="/templates/089-standard/css/normalize.css" rel="stylesheet" type="text/css" />
	<link href="/templates/089-standard/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="/templates/089-standard/css/overrides.css" rel="stylesheet" type="text/css" />	
</head>

<body id="body" class="site <?php print $detectAgent . ($detect->isMobile() ? "mobile " : " ") . $body_class . ($layout ? $layout." " : '') . $option. ' view-' . $view. ($itemid ? ' itemid-' . $itemid : ''); ?>">
	<!-- Body -->
		<div class="fullwidth site_wrapper">
			<div class="aboveTheFoldWrapper">
				<?php							
				// including header
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/header.php');			
				// including breadcrumb
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/breadcrumbs.php');																	
				// including content
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/content.php');
				?>
			</div>
			<?php
			if ($this->countModules('bottom1')) {					
				// including bottom
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/bottom.php');					
			}
			if ($this->countModules('bottom7')) {					
				// including bottom
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/bottom2.php');					
			}
			if ($this->countModules('bottom13')) {					
				// including bottom
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/bottom3.php');					
			}			
				// including footer
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/footer.php');								
				?>
			
		</div>
	
	
	<jdoc:include type="modules" name="debug" style="none" />

	<script src="/templates/089-standard/js/jquery.lazy.min.js" type="text/javascript" defer></script>
	<script src="/templates/089-standard/js/jquery.lazy.iframe.min.js" type="text/javascript" defer></script>
	<script type="text/javascript">

	jQuery(document).ready(function() {
		<?php if($detectAgent =="tablet ") : ?>//tablet menu open/close ?>
			jQuery('.btn-navbar').click("on", function() {
				jQuery('.nav-collapse.collapse').toggleClass('openMenu');
			});
		<?php endif;?>

		<?// lazy load: ?>
			jQuery('.lazy').lazy({ 
					threshold: 10,
					visibleOnly: true,
					afterLoad: function(element) {
		            	jQuery('.bottom').addClass('loaded');
		            	jQuery('.bottom3').addClass('loaded');	            	
		        	}
				});
		<?//<-- END lazy load: ?>
			<?php // scroll detection sticky menu und scrollVisible: ?>
				jQuery(window).scroll(function() {
					<?php if($detectAgent == "desktop ") : ?>
						(jQuery(this).scrollTop() > 100 ) ? jQuery('#header').addClass('sticky') : jQuery('#header').removeClass('sticky') && jQuery('.innerwidth.content').css('margin-top', '0px');
					<?php endif; ?>
			<?php // scrollVisible Variablen: ?>
				<?php if($pageclass == " home") : // scrollVisible nur auf Unterseiten wo diese benötigt werden sonst-> JS error in Konsole ?>
				    var top_of_element = jQuery(".scrollVisible").offset().top;
				    var bottom_of_element = jQuery(".scrollVisible").offset().top + jQuery(".scrollVisible").outerHeight();
				    var bottom_of_screen = jQuery(window).scrollTop() + jQuery(window).height();
				    var top_of_screen = jQuery(window).scrollTop();
					    if((bottom_of_screen > top_of_element) && (top_of_screen < bottom_of_element)) {
					    	setTimeout(function() {
					        	jQuery('.scrollVisible').addClass('loaded');
					    	}, 750);
					    }
				<?php endif; ?>				    
					});
			<?php //<--- END scroll detection sticky menu und scrollVisible: ?>

		<?php if($pageclass == " team" && !$clientPhone) : ?> <?php //CG:  video start / pause nur auf team-seite ?>
			jQuery(window).scroll(function(){
				(jQuery(this).scrollTop() > 200) ? jQuery('video').get(0).play() : jQuery('video').get(0).pause();
			});
		<?php endif;?>				
	});
	</script>
<?php if(!$clientMobile) : ?>
	<div id="resizeAlarm">
		<p>Das Fenster Ihres Webbrowsers ist zu klein - bitte vergrössern Sie ihr Browser-Fenster um die Inhalte sinnvoll darstellen zu können.</p>
	</div>
<?php endif; ?>	
</body>
</html>
