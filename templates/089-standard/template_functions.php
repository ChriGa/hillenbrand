<?php

/**
 * Hier werden Hilfsfunktionen und Variablen definiert die Systemweit durch include
 * in der index.php verwendbar und aufrufbar sind
 * Author: CG
 **/

/**
 * Debuggingausgabe von Objekten und Arrays
 **/	
/*	function preprint($s, $where="") {
		print "<pre>";
			if ($where) print $where."<br/>";
			print_r($s);
		print "</pre>";
	}*/

// Sind wir auf der Startseit?: http://docs.joomla.org/How_to_determine_if_the_user_is_viewing_the_front_page
$menu = JFactory::getApplication()->getMenu();
$currentMenuID = $menu -> getActive()->id;
//print_r("Menue ist $currentMenuID");
$body_class = " is_home";
	if ($currentMenuID != 111 ) { $body_class = " not_home"; }

/* pageclass_sfx überall verfügbar machen */

	$beitragsID = JRequest::getVar('Itemid');
	$menu4PageClass = &JSite::getMenu();
	$activeBeitrag = $menu4PageClass->getItem($beitragsID);
	$paramsBeitrag = $menu4PageClass->getParams( $activeBeitrag->id );
if (!empty($paramsBeitrag->get( 'pageclass_sfx' ))) {
	$pageclass = $paramsBeitrag->get( 'pageclass_sfx' );
} else {
	$pageclass = " ";
}

/**
 * Meta Generator löschen und 089-webdesign eintragen
 **/
	unset($headMetaData["metaTags"]["standard"]["title"]); 
	$this->setGenerator('089-WEBDESIGN'); 


/*erweiterte Mobile Detection siehe: http://mobiledetect.net/*/

		require_once 'Mobile_Detect.php';
		$detect = new Mobile_Detect;
//mobile detect variablen:
if (!$detect->isMobile()) {	$detectAgent = "desktop "; $clientMobile = false; }
elseif (!$detect->isMobile() || $detect->isTablet()) {	$detectAgent = "tablet "; $clientMobile = true; } 
else { $detectAgent = "phone "; $clientMobile = true; $clientPhone = true; }		

/*
 * error-reporting nur wenn Entwicklung local oder live mit eigener IP
 * Immerwieder erneuern! IP oder HTTP-HOST (joomla Version zB)
 */
		
		$user = 'none';

		if ($_SERVER["REMOTE_ADDR"] == "84.140.111.200") {
				$user = "ftp_dev";
		}

		if ($_SERVER["HTTP_HOST"] == "localhost:8888") {
				$user = "local_dev";
		}

			define('dev_umgebung', $user);

		switch(dev_umgebung) {
			case 'ftp_dev':
				error_reporting(E_ALL ^ E_NOTICE);
				ini_set('display_errors', '1');
				break;
			case 'local_dev':
				error_reporting(E_ERROR);
				ini_set('display_errors', '1');
				break;
			case 'none':
				error_reporting(0);
				break;
			default:
				exit('Error-Reporting User wurde nicht korrekt initialisiert!');
				@mail("cg@089webdesign.de", "Vorsicht, bei ".$_SERVER["HTTP_HOST"], __FILE__."\n\nwurde kein User gesetzt für das Error-Reporting!");
		}

/**
 * Für bestimmte Browser im Body eine Klasse setzen, um CSS-Styles setzen zu können
 **/
	$browser = getBrowser(); // siehe /defines.php

	// Name des Browsers
	$br_name = $browser["name"];
	if ($br_name=="Mozilla Firefox") $br_name = "Firefox";
	$br_platform = $browser["platform"];
	
	$browser = $br_name." ".$br_platform." "; // zB "InternetExplorer InternetExplorer90" oder "Firefox Firefox180"
	// Add the menuitem Pageclass-Suffix to the body-class
	$body_class .= " ".$browser." ";
	if($active_menu_item) {
		$body_class .= $active_menu_item->params->get( 'pageclass_sfx' );
	}

?>