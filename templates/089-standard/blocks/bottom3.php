<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 

defined('_JEXEC') or die;

?>
<div class="bottom3 fullwidth">
	<div class="row-fluid">
		<div class="span12">
			<jdoc:include type="modules" name="bottom13" style="custom" />
		</div>
		<?php if($this->countModules('bottom14') && !$clientPhone) : ?>
			<div class="span12 teamVideoMargin">
				<jdoc:include type="modules" name="bottom14" style="custom" />
			</div>
		<?php endif; ?>
	</div> 	
</div>