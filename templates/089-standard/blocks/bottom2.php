<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 

defined('_JEXEC') or die;

$poss = array('bottom7','bottom8','bottom9','bottom10','bottom11','bottom12');
$n = 0;
if ($this->countModules('bottom7')) $n++;
if ($this->countModules('bottom8')) $n++;
if ($this->countModules('bottom9')) $n++;
if ($this->countModules('bottom10')) $n++;
if ($this->countModules('bottom11')) $n++;
if ($this->countModules('bottom12')) $n++;

if ($n > 0) {
$span = 12/$n;
?>
	<div class="bottom2 scrollVisible fullwidth">
		<div class="bottom2-wrap innerwidth">
			<div class="row-fluid">
				<?php foreach ($poss as $i => $pos): ?>
					<?php if ($this->countModules($pos)) : ?>
					<div class="span<?php echo $span; ?> module_bottom position_<?php echo $pos; ?>">
						<jdoc:include type="modules" name="<?php echo $pos ?>" style="custom" />
					</div>
					<?php endif ?>
				<?php endforeach ?>
			</div> 	
		</div> 
	</div>	
<?php } ?>		
		
