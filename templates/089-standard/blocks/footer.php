<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<footer class="footer fullwidth">
	<div class="footer-wrap">															
		<jdoc:include type="modules" name="footer" style="custom" />		
	</div>
</footer>
<div id="copyright" class="fullwidth">
	<div class="copyWrapper innerwidth">
		<p>&copy; <?php print date("Y");?> | <a class="imprLink" href="/impressum.html" title="Impressum Spedition Hillenbrand">Impressum</a> | <a class="imprLink" href="/datenschutz.html" title="Impressum Spedition Hillenbrand">Datenschutz</a></p>
	</div>
</div>	
		