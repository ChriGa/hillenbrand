<?php 

/**
 * @package 	mod_ap_ajax_quick_contact.php - AP Ajax Quick Contact Module
 * @version		3.3
 * @author		Aplikko
 * @email		contact@aplikko.com
 * @website		http://aplikko.com
 * @copyright	Copyright (C) 2014 Aplikko.com. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
**/

// no direct access
defined('_JEXEC') or die;

/* additional style for responsive */ ?>
<style type="text/css">
#ap_ajax_qc_<?php echo $uniqid; ?> label, 
#ap_ajax_qc_<?php echo $uniqid; ?> input.dsgvoCheckBox {width:<?php echo $form_width / 100 * 20; ?>%;}
#ap_ajax_qc_<?php echo $uniqid; ?> input,
#ap_ajax_qc_<?php echo $uniqid; ?> textarea,
#ap_ajax_qc_<?php echo $uniqid; ?> .error {width:<?php echo $form_width / 100 * 76; ?>%}
#ap_ajax_qc_<?php echo $uniqid; ?> .error {margin:2px 0 0 <?php echo $form_width / 100 * 21.5; ?>%;}
#ap_ajax_qc_<?php echo $uniqid; ?> input#captcha {width:<?php echo $form_width / 100 * 17; ?>%;}
#ap_ajax_qc_<?php echo $uniqid; ?> input.dsgvoCheckBox {width: 30px !important; margin-top: 10px !important; margin-left: <?php print $form_width / 100 * 20;?>% !Important; box-shadow: none !important; }
#ap_ajax_qc_<?php echo $uniqid; ?> .datenschutzHint { font-size: 14px; margin-left: calc(<?php print $form_width / 100 * 20;?>% + 40px); padding-left: 30px; margin-bottom: 20px; color: #777;}
#ap_ajax_qc_<?php echo $uniqid; ?> .datenschutzHint a { text-decoration: underline; }
#ap_ajax_qc_<?php echo $uniqid; ?> label#dsgvo_label, .datenschutzHint { width: 70%; }
#ap_ajax_qc_<?php echo $uniqid; ?> label#dsgvo_label { width: 70%; text-align: justify; font-size: 16px; }
#ap_ajax_qc_<?php echo $uniqid; ?> .input.dsgvo_check { margin: 30px 0px 20px;}
#ap_ajax_qc_<?php echo $uniqid; ?> label#dsgvo_label { margin-left: 0; }
body#body.tablet #ap_ajax_qc_<?php echo $uniqid; ?> .datenschutzHint { margin-left: <?php print $form_width / 100 * 20;?>% ; }
body#body.tablet #ap_ajax_qc_<?php echo $uniqid; ?> label#dsgvo_label {  width: 70%; font-size: 14px;}
body#body.tablet #ap_ajax_qc_<?php echo $uniqid; ?> .datenschutzHint {  width: 70%; font-size: 14px;}

@media (max-width: 767px) {
body#body.mobile #ap_ajax_qc_<?php echo $uniqid; ?> label#dsgvo_label { width: 50%; text-align: justify; font-size: 14px; }
body#body.mobile #ap_ajax_qc_<?php echo $uniqid; ?> label#dsgvo_label, .datenschutzHint { width: 80%; }
body#body.phone #ap_ajax_qc_<?php echo $uniqid; ?> .datenschutzHint { margin-left: 0; padding: 5px; width: 100%; }
#ap_ajax_qc_<?php echo $uniqid; ?> .datenschutzHint { margin-left: 0; padding-left: 30px; }
body#body.mobile #ap_ajax_qc_<?php echo $uniqid; ?> input.dsgvoCheckBox {width: 30px !important; margin-left: 0 !Important; }
#ap_ajax_qc_<?php echo $uniqid; ?> label {width:15%}
#ap_ajax_qc_<?php echo $uniqid; ?> input,
#ap_ajax_qc_<?php echo $uniqid; ?> textarea,
#ap_ajax_qc_<?php echo $uniqid; ?> .error {width:75%;}
#ap_ajax_qc_<?php echo $uniqid; ?> .error {margin:2px 0 0 17%;}
#ap_ajax_qc_<?php echo $uniqid; ?> input#captcha {width:17%;}
}
@media (max-width: 480px) {
#ap_ajax_qc_<?php echo $uniqid; ?> label {width:92%;}
#ap_ajax_qc_<?php echo $uniqid; ?> input,.ap_ajax_qc textarea,.ap_ajax_qc .error {width:92%;}
#ap_ajax_qc_<?php echo $uniqid; ?> .error {margin:2px 0 0 12px;}
#ap_ajax_qc_<?php echo $uniqid; ?> input#captcha {width:80px;}
#ap_ajax_qc_<?php echo $uniqid; ?> .for_captcha label{width:50px;}
}
</style>
<?php

if($recipient == "") { ?>
<div class="ap_contact_alert row-fluid">
       <div align="center" class="span12" style="text-align:center;">
             <div class="alert alert-block fade in">
              <button type="button" class="close" data-dismiss="alert">&times;</button>
                <div id="message">
                    <h4>
                    <span class="label label-important">
                    <i class="fa fa-info-circle"></i>
                    <?php echo JText::_(MOD_AP_AJAX_QUICK_CONTACT_IMPORTANT); ?>
                    </span>
                     <?php echo JText::_(MOD_AP_AJAX_QUICK_CONTACT_EMAIL_NOT_SET); ?>
                    </h4>
                    <p><?php echo JText::_(MOD_AP_AJAX_QUICK_CONTACT_EMAIL_NOT_SET_REMINDER); ?></p>
                </div>
              </div>
      </div>  
</div>
<?php } else { ?>
<div id="ap_ajax_qc_<?php echo $uniqid; ?>" class="ap_ajax_qc <?php echo $moduleclass_sfx ?> row-fluid">
	        <?php if(isset($emailSent) && $emailSent == true) { ?>
                <div class="success span12"><?php echo $ap_send_message=='' ? JText::_(SENDMESSAGE) : $ap_send_message; ?></div>
		<?php } else { ?>
        <form id="ap_<?php echo $uniqid; ?>" class="ap_ajaxqc" action="<?php echo JURI::current(); ?>" method="post">
                    <div class="input">
                        <label for="name"><?php echo $name; ?></label>
                        <input type="text" name="ap_name" id="name" value="<?php if(isset($_POST['ap_name'])) echo $_POST['ap_name'];?>" <?php /*class="requiredField" */?> placeholder="<?php echo $name. " (optional)"; ?>" />
                        <?php if($nameError = '') { ?><div class="error"><?php echo $nameError;?></div><?php } ?>
                    </div>
                    <div class="input">
                        <label for="email"><?php echo $email; ?></label>
                        <input type="text" name="ap_email" id="email" value="<?php if(isset($_POST['ap_email']))  echo $_POST['ap_email'];?>" class="email requiredField" placeholder="<?php echo $email; ?>" />
                        <?php if($emailError = '') { ?><div class="error"><?php echo $emailError;?></div><?php } ?>
                    </div>
                    <div class="input for_message">
                        <label for="message"><?php echo $message; ?></label>
                        <textarea name="ap_message" id="message" class="requiredField" rows="6" placeholder="<?php echo $message; ?>"><?php if(isset($_POST['ap_message'])) { if(function_exists('stripslashes')) { echo stripslashes($_POST['ap_message']); } else { echo $_POST['ap_message']; } } ?></textarea>
                        <?php if($messageError = '') { ?><div class="error" id="error"><?php echo $messageError;?></div><?php } ?>
                    </div>
                    <?php if ($captcha_label == "1") {?>
                    <div class="input for_captcha">
                      <label for="captcha"><?php echo $captcha; ?></label> <span class="nmbr"><?php echo $_SESSION['n1']?> + <?php echo $_SESSION['n2']?> =</span>
                      <input type="text" class="requiredCaptcha" name="ap_captcha" id="captcha" value="<?php if (isset($_POST['ap_captcha'])) echo ($_POST['ap_captcha']); ?>" placeholder="<?php echo $captcha; ?>"/>
                      <?php if($captchaError = '') { ?><div class="error"><?php echo $captchaError;?></div><?php } ?>
                    </div>
                    <?php } ?>

                    <div class="input dsgvo_check">
                      <input class="dsgvoCheckBox" required type="checkbox" name="dsgvo_checkbox" value="<?php if(isset($_POST['dsgvo_checkbox'])) print $_POST['dsgvo_checkbox'];?>" />
                      <label for="dsgvo" id="dsgvo_label" name="Datenschutzhinweis">
                          <?php print $dsgvo_text; ?>
                      </label>                  
                    </div>
                    <p class="datenschutzHint">Detaillierte Informationen zum Umgang mit Nutzerdaten finden Sie in unserer <a href="/datenschutz">Datenschutzerkl&auml;rung</a></p>
                    <div class="input for_submit">
                     <label></label>
                       <button name="submit" type="submit" class="ap-btn">
                        <span><?php /**loader span**/ ?></span>
                        <i class="fa fa-share"></i>
                        <?php echo $submit; ?>
                       </button>
                      <input type="hidden" name="submitted" id="submitted" value="true" />
                    </div>
                </form>			
        <?php } ?>
</div>
<?php } ?>
<script type="text/javascript">
  document.addEventListener("DOMContentLoaded", function() {
      var elements = document.getElementsByTagName("INPUT");
      for (var i = 0; i < elements.length; i++) {
          elements[i].oninvalid = function(e) {
              e.target.setCustomValidity("");
              if (!e.target.validity.valid) {
                  e.target.setCustomValidity("Bitte klicken Sie dieses Kästchen an und bestätigen Sie, dass Sie unsere Datenschutzerklärung zur Kenntnis genommen haben.");
              }
          };
          elements[i].oninput = function(e) {
              e.target.setCustomValidity("");
          };
      }
  });
</script>